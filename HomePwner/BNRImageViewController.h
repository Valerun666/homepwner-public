//
//  BNRImageViewController.h
//  HomePwner
//
//  Created by Valerun on 28.12.14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRImageViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *imageView;
//@property (nonatomic, strong) UIScrollView *sv;

@end
