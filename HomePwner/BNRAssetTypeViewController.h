//
//  BNRAssetTypeViewController.h
//  HomePwner
//
//  Created by Admin on 03.02.15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BNRItem;

@interface BNRAssetTypeViewController : UITableViewController

@property (strong, nonatomic) BNRItem *item;

@end
