//
//  BNRImageViewController.m
//  HomePwner
//
//  Created by Valerun on 28.12.14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "BNRImageViewController.h"

@interface BNRImageViewController () <UIScrollViewDelegate>

@end

@implementation BNRImageViewController

- (void)loadView
{
    self.imageView = [[UIImageView alloc]initWithImage:self.image];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.imageView setCenter:CGPointMake(600 / 2.0, 600 / 2.0)];
    
    UIScrollView *sv = [[UIScrollView alloc]initWithFrame:self.imageView.frame];
    sv.bouncesZoom = YES;
    sv.zoomScale = 0.5;
    sv.maximumZoomScale = 6.0;
    sv.minimumZoomScale = 0.5;
    sv.contentSize = self.imageView.frame.size;
//    sv.scrollEnabled = NO;
    sv.delegate = self;
    
    [sv addSubview:self.imageView];
    
    self.view = sv;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    
    [self.view addSubview:self.imageView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
